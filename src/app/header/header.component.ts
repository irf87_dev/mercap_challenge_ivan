import { Component, OnInit,OnChanges, Input, Output, EventEmitter } from '@angular/core';

//Own imports
import { HeaderService } from './header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers : [HeaderService]
})

export class HeaderComponent implements OnInit {
  //Variables
  public currentItem : string = "";
  public oApi : any = { };
  //Entradas
  @Input() item : string = "";
  @Input() working : boolean;

  //Salidas
  @Output() itemSelected = new EventEmitter();

  constructor( private service : HeaderService) { }

  ngOnInit() {
    this.service.get().subscribe(
      (data) => {
        this.oApi = this.service.parseObjet(data);
        this.chooseItem(this.oApi.keys[0]);
      },
      (error) =>{
        console.error(error);
        alert("Ocurio un error al obtener los datos");
      }
    );
  }

  ngOnChanges(changes){
    if(changes.item){
      if(changes.item.currentValue != ""){
        this.currentItem = changes.item.currentValue;
      }
    }
  }

  chooseItem(currentItem : string){
    if(this.working == false){
      this.currentItem = currentItem;
      this.itemSelected.emit(currentItem);
    }
    else alert("Existe una operación en proceso");
  }

}
