import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
@Injectable()
export class HeaderService {
  private host: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  get() {
    return this.http.get<any>(this.host, {});
  }

  parseObjet(api: any) {
    let arrayKeys = Object.keys(api);
    let oApi = {
      object: api,
      keys: arrayKeys.sort()
    };

    return oApi;
  }
}
