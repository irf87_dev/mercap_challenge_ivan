import { Component, OnInit, OnChanges , Input, Output, EventEmitter  } from '@angular/core';

import { ItemService } from './items.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css'],
  providers : [ItemService]
})
export class ItemsComponent implements OnInit {
  public currentItem : any = {};
  public species : string = "";

  @Input() data : any = {};
  @Input() detailMode : boolean = false;
  @Input() itemType : string = "";

  @Output() detailChange = new EventEmitter();
  @Output() itemChange = new EventEmitter();

  constructor(private service : ItemService) { }

  ngOnInit() {
  }

  ngOnChanges(changes){

  }

  selectCategory(item){
    if(item == "films" || item == "people"){
      let keyUrl = "films";
      if(item == "people") keyUrl = "characters";
      let object = {
        item : item,
        url : this.currentItem[keyUrl]
      }
      this.itemChange.emit(object);
    }
    else alert("Very soon you will see the data of " + item);
  }

  viewDetail(item){
    this.currentItem = item;
    const emit = function(context){
      let value = "";

      if(context.itemType == 'people') value = item.name;
      else value = item.title;

      let object = {
        componentName : context.itemType,
        value : value,
        detailMode : true
      };
      context.detailChange.emit(object);
    };

    if(this.itemType == 'people'){
      this.service.get(item.species[0]).subscribe(
        (data) =>{
          this.species = data.name;
          emit(this);
        },
        (error) =>{
          this.species = "Desconocida";
          alert("Error al conseguir species");
        }
      );
    }
    else emit(this);
  }

}
