import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
@Injectable()
export class ItemService {
  private host:string = environment.apiUrl;

  constructor(private http:HttpClient ){}

  get(url){
    return this.http.get<any>(url,{});
  }

}
