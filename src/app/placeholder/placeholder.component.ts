import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

import { PlaceholderService } from './placeholder.service';

@Component({
  selector: 'app-placeholder',
  templateUrl: './placeholder.component.html',
  styleUrls: ['./placeholder.component.css'],
  providers: [PlaceholderService]
})
export class PlaceholderComponent implements OnInit {
  public oParams: any = {};
  public data: any = {};

  public nestedBreadcrumb: string = "";
  public modeDetail: boolean = false;
  public showLoader : boolean = false;

  //Entradas
  @Input() itemSelected: string = "";

  //Salidas
  @Output() forceSelectItem = new EventEmitter();
  @Output() working = new EventEmitter();

  constructor(private service: PlaceholderService) { }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (changes.itemSelected) {
      this.clean();
      if (this.service.makeRequest(this.itemSelected)) {
        this.getData();
      }
    }
  }

  clean() {
    this.data = {};
    this.modeDetail = false;
    this.nestedBreadcrumb = "";
  }

  getData() {
    this.showLoader = true;
    //console.log("Debe emitir")
    this.working.emit(this.showLoader);
    this.service.get(this.itemSelected, {}).subscribe(
      (data) => {
        this.showLoader = false;
        this.working.emit(this.showLoader);
        this.data = data;
      },
      (error) => {
        this.showLoader = false;
        this.working.emit(this.showLoader);
        console.error(error);
        alert("Ocurrio un error");
      }
    );
  }

  navigate(url) {
    this.data = {};
    this.showLoader = true;
    this.working.emit(this.showLoader);
    this.service.page(url).subscribe(
      (data) => {
        this.showLoader = false;
        this.working.emit(this.showLoader);
        this.data = data;
      },
      (error) => {
        this.showLoader = false;
        this.working.emit(this.showLoader);
        console.error(error);
        alert("Ocurrio un error");
      }
    );
  }

  getArrayUrl(urlArray) {
    let iTam = urlArray.length;
    let iCont = 0;
    let array = [];
    let context = this;
    this.data.results = [];

    const makeRequest = function(url, callBack) {
      context.service.page(url).subscribe(
        (data) => {
          array.push(data);
          callBack();
        },
        (error) => {
          console.error(error);
          callBack();
        }
      );
    }

    const recursive = function(url) {
      iCont++;
      if (iCont <= iTam) {
        makeRequest(url, () => {
          recursive(urlArray[iCont]);
        })
      }
      else {
        context.showLoader = false;
        context.working.emit(context.showLoader);
        context.data.results = array;
      }
    }
    this.showLoader = true;
    this.working.emit(this.showLoader);
    recursive(urlArray[iCont]);
  }

  parentBreadcrumb() {
    this.modeDetail = false;
    this.nestedBreadcrumb = "";
  }

  onDetailMode(obj) {
    this.modeDetail = obj.detailMode;
    this.nestedBreadcrumb = obj.value;
  }

  onItemChange(item) {
    this.clean();
    this.itemSelected = item.item;
    this.forceSelectItem.emit(item.item);
    if (item.url.length > 0) {
      this.getArrayUrl(item.url);
    }
  }

}
