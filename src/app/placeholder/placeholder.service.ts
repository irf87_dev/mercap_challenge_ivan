import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';

@Injectable()
export class PlaceholderService {
  private host:string = environment.apiUrl;

  constructor(private http:HttpClient ){}

  get(endpoint : string, oParams : any){
    endpoint += "/";
    return this.http.get<any>( this.host + endpoint, { params : oParams } );
  }

  page(endpoint : string){
    return this.http.get<any>( endpoint, { } );
  }

  makeRequest( item : string ){
    let bMake = false;
    if(item == "people" || item == "films") bMake = true;
    return bMake;
  }

  getPrimaryKey(item){
    let key = "";
    switch(item){
      case 'people':
        key = "name"
      break;

      case 'films':
        key = "title"
      break;
    }
    return key;

  }
}
