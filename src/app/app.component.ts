import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public itemSelected : string = "";
  public forceItem : string = "";
  public isWorking : boolean;
  public bPreviousForce : boolean = false;

  constructor() {  }

  ngOnInit() {
    this.isWorking = false;
  }

  itemHasBeenSelected(item : string){

    if(this.bPreviousForce == true){
        this.itemSelected = "";
        this.bPreviousForce = false;
        setTimeout(()=>{
          this.itemSelected = item;
          this.forceItem = "";
        },50);
    }
    else{
      this.itemSelected = item;
      this.forceItem = "";
    }
  }

  onForceSelectItem(item){
    this.bPreviousForce = true;
    this.forceItem =  item;
  }

  onWorking(value){
    setTimeout(()=>{
      this.isWorking = value;
    },50);
  }


}
