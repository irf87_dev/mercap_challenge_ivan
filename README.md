#Estructura del proyecto.
- Src contiene todos los archivos del proyecto.
- Src > app contiene todos los modulos que se utilizan: componentes, directivas, filtros (pipes), etc
- Src > assets contiene archivos extra multimedia como imagenes, fuentes etc, en este caso no aplica.
- Src > environments contiene constantes que se utilizan en todo el proyecto, maneja un archivo de producción y otro de desarrollo, útil para colocar las url de las apis.
- Src > style.css Estilo general para todo el proyecto, aun que cada modulo contiene su archivo css, este se maneja cuando es estilo abarca todo el proyecto.

src > app
El modulo principal es:
  - app.component.ts
  - app.component.html
Este modulo contiene los sub componentes del sistema.

Los sub modulos son:
- src > app > header: Controla el header de la página
- src > app > placeholder : Recibe la selección del header y obtiene los datos

Sub componente del placeholder
- src > app items : Muestra los datos

Dentro de la carpeta pipes, contiene todos los filtros utilizados en el sistema, en este caso es solo uno.

Dentro de cada componente la arquitectura es la siguiente:
.css Contiene estilos exclusivos del componente, a diferencia del style.css, este solo aplica para elementos del componente.
.html La vista del componente.
.ts Es el controlador del componente.
.service.ts Contiene lógica extra y obtención de datos.

Los demás archivos son propios de angular.

#Justificación de tecnologías.
Utilice Angular porque es un framework muy completo y robusto, por la nataturaleza del framework, el uso de componentes ayuda a reutilización del código y modularización del mismo, permitiendo un código más limpio y reutilizable. Además angular-cli, ya contiene las herramientas necesarias para preparar el proyecto para producción sin necesisdad de agregar más librerias o configuraciones.

[Link](http://sisnesw.com/mercap/)
